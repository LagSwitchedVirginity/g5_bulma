# G5 Bulma Package #

## What is this about? ##
The goal of this is to transform [Bulma v5.4.6] for [Gantry] into a [Bulma] theme for [Grav] \w [Gantry]!

## Why in hell would I need that? ##
I mean why not? Seriously why not?
We, or heck even you, could use this to make a Bootstrap based one! Or even foundation!
The fact is simple, why have only themes you have to pay for except 2, and those 2 are great, don't get me wrong!
But there are different CSS-Frameworks out there that are more or less extendible!

## Stops along the road to the finish line! ##
1. Setting up the structure!
2. Creating this shitty README file



[Gantry]: http://gantry.org/ "Gantry"
[Grav]: https://getgrav.org/ "Grav"
[Bulma v5.4.6]: https://github.com/gantry/gantry5/releases/download/5.4.6/grav-tpl_g5_bulma_v5.4.6.zip "Bulma v5.4.6"
[Bulma]: http://bulma.io/ "Bulma"
[CSS-Frameworks]: http://cssframeworks.org/ "CSS-Frameworks Site"